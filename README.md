Hugues Marti

Commande docker compose :

    docker-compose up -d --build 


Routes Vue Js :

    Liste des todolists : localhost:8080/todolists

    Login : localhost:8080/login     (login admin : "joe" | password admin : "pass")

    Register : localhost:8080/register


Routes api Flask :

    Login : localhost:5000/login/{username}/{password} GET

    Register : localhost:5000/register/{username}/{password} POST

    Liste de todolists : localhost:5000/todolists GET/POST

    Une todolist particuliere : localhost:5000/todolists/{todolist_id} PUT/DELETE/GET 

    Les todos d'une todolist : localhost:5000/todolist/{todolist_id} GET/POST

    Un todo particulier : localhost:5000/todolist/{todolist_id}/{todo_id} PUT/DELETE/GET

    Token actuel : localhost:5000/token GET

    Logout : localhost:5000/logout GET



