from flask import Flask, jsonify, request, make_response, current_app
from flask_cors import CORS
import jwt
from werkzeug.security import safe_str_cmp
import uuid
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from functools import wraps
import json
from calendar import timegm
from todolists import TODOLISTS

# configuration
DEBUG = True

class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id

    @classmethod
    def authenticate(cls, **kwargs):
        username = kwargs.get('username')
        password = kwargs.get('password')
        
        if not username or not password:
            return None

        for user in users:
            if user.username == username :
                current_user = user
                break

        if not current_user or not check_password_hash(current_user.password, password):
            return None
        
        return current_user

    @classmethod
    def addUser(cls, **kwargs):
        username = kwargs.get('username')
        password = kwargs.get('password')
        users.append(User(uuid.uuid4().hex, username, generate_password_hash(password)))

users = [
    User(uuid.uuid4().hex, 'joe', 'pbkdf2:sha256:150000$wZwrjBcj$9eb110398e902dd4bad3ab5b809d06f8802d8f03f9f087ff8a900fe5e502f063'),
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

app.config['SECRET_KEY']='super-secret'

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

def resetToken():
    with open('token.json', 'w') as outfile:
        outfile.write("")

resetToken()

def saveTodoLists():
     with open('todolists.py', 'w') as outfile:
        liste = ""
        for todolist in TODOLISTS:
            liste += json.dumps(todolist,ensure_ascii=False) + ",\n"
        outfile.write("import uuid\n\nTODOLISTS = [\n"+liste+"]")


def getToken():
    with open('token.json') as json_file:
        try :
            TOKEN = json.load(json_file)
        except :
            return False
    try :
        token = jwt.decode(TOKEN, current_app.config['SECRET_KEY'])
        return token['exp'] > timegm(datetime.utcnow().utctimetuple()) 
    except jwt.ExpiredSignatureError:
        return False

@app.route('/todolists', methods=['GET','POST'])
def all_todolists():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        TODOLISTS.append({
            'id': uuid.uuid4().hex,
            'title': post_data.get('title'),
            'todos': []
        })
        saveTodoLists()
        response_object['message'] = 'Todo List added!'
    else:
        response_object = {'status': 'error'}
        response_object['todolists'] = TODOLISTS
    return jsonify(response_object)

def remove_todolist(todolist_id):
    for todolist in TODOLISTS:
        if todolist['id'] == todolist_id:
            TODOLISTS.remove(todolist)
            return True
    return False

def findTodolist(todolist_id):
    for todolist in TODOLISTS:
        if todolist['id'] == todolist_id:
            return todolist

@app.route('/todolists/<todolist_id>', methods=['PUT','DELETE','GET'])
def single_todolist(todolist_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        todolist = findTodolist(todolist_id)
        remove_todolist(todolist_id)
        TODOLISTS.append({
            'id': uuid.uuid4().hex,
            'title': post_data.get('title'),
            'todos': todolist['todos']
        })
        saveTodoLists()
        response_object['message'] = 'Todolist updated!'
    if request.method == 'GET':
        response_object['todolist'] = findTodolist(todolist_id)    
    if request.method == 'DELETE':
        remove_todolist(todolist_id)
        saveTodoLists()
        response_object['message'] = 'Todolist removed!'
    return jsonify(response_object)

@app.route('/todolist/<todolist_id>', methods=['GET','POST'])
def all_todos(todolist_id):
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        todolist = findTodolist(todolist_id)
        todolist['todos'].append({
            'id': uuid.uuid4().hex,
            'texte':post_data.get('title')
            })
        saveTodoLists()
        response_object['message'] = 'Todo added!'
    else:
        response_object['todos'] = todolist
    return jsonify(response_object)


@app.route('/todolist/<todolist_id>/<todo_id>', methods=['PUT','DELETE','GET'])
def single_todo(todolist_id,todo_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        todolist = findTodolist(todolist_id)
        remove_todo(todolist_id,todo_id)
        todolist['todos'].append({
            'id': uuid.uuid4().hex,
            'texte':post_data.get('title')
        })
        saveTodoLists()
        response_object['message'] = 'Todolist updated!'   
    if request.method == 'DELETE':
        remove_todo(todolist_id,todo_id)
        saveTodoLists()
        response_object['message'] = 'Todo removed!'
    return jsonify(response_object)


def remove_todo(todolist_id, todo_id):
    todolist = findTodolist(todolist_id)
    for todo in todolist['todos']:
        if todo['id'] == todo_id:
            todolist['todos'].remove(todo)
            return True
    return False    

@app.route('/token', methods=['GET'])
def token():
    response_object = {'status': 'success'}
    response_object['istokenvalid'] = getToken()
    return response_object

@app.route('/login/<username>/<password>', methods=['GET'])
def login(username,password):
    response_object = {'status': 'success'}
    user = User.authenticate(**{"username" : username, "password" : password})

    if not user:
        return jsonify({ 'message': 'Invalid credentials', 'authenticated': False }), 401

    token = jwt.encode({
        'sub': user.username,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(minutes=30)},
        current_app.config['SECRET_KEY'])
    with open('token.json', 'w') as outfile:
        json.dump(token.decode('UTF-8'), outfile)    
    return jsonify({ 'token': token.decode('UTF-8') })

@app.route('/logout', methods=['GET'])
def logout():
    response_object = {'status': 'success'}
    response_object['message'] = 'Logout successful'
    resetToken()
    return response_object

@app.route('/register/<username>/<password>', methods=['POST'])
def register(username,password):
    response_object = {'status': 'success'}
    User.addUser(**{"username" : username, "password" : password})
    response_object['message'] = 'User Added !'
    return jsonify(response_object)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, use_reloader=False)