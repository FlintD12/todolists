import Vue from 'vue';
import Router from 'vue-router';
import Todolists from './components/Todolists.vue';
import Todolist from './components/Todolist.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/todolists',
      name: 'Todolists',
      component: Todolists,
      meta: { requiresAuth: true },
    },
    {
      path: '/todolist/:id',
      name: 'Todolist',
      component: Todolist,
      meta: { requiresAuth: true },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:5000/token';
    Http.open('GET', url);
    Http.send();

    Http.onreadystatechange = () => {
      if (Http.readyState === 4 && Http.status === 200) {
        if (!JSON.parse(Http.responseText).istokenvalid) {
          next({
            name: 'Login',
            query: { redirect: to.fullPath },
          });
        } else {
          next();
        }
      }
    };
  } else {
    next();
  }
});

export default router;
